package com.corsoos.studentesrvMongo.controller;


public class Libro {

	private String id;
	private String titolo;
	private String autore;
	
	public Libro() {
		
	}

	public Libro(String titolo, String autore) {
		super();
		this.titolo = titolo;
		this.autore = autore;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitolo() {
		return titolo;
	}
	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}
	public String getAutore() {
		return autore;
	}
	public void setAutore(String autore) {
		this.autore = autore;
	}
	
}