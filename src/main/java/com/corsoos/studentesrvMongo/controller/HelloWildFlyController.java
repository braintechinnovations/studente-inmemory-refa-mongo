package com.corsoos.studentesrvMongo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.corsoos.studentesrvMongo.model.Course;
import com.corsoos.studentesrvMongo.service.StudentService;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

@RestController
public class HelloWildFlyController {


    @RequestMapping("test")
    public String sayHello(){
        return ("Hello, try to MongoTheWorld!");
    }
    
    @Autowired
	private StudentService studentService;

    @RequestMapping("fillit")
    public String fillTheMongo(){
    	MongoClientURI uri_connessione = new MongoClientURI("mongodb://admin:toor@mongodb-new/sampledb");	//Definire un URI di connessione
    	MongoClient client = new MongoClient(uri_connessione);								//Definisco il cliente che si occuperà di convogliare tutte le istruzioni al DB
    	
    	DB database = client.getDB("sampledb");												//use Libreria
    	DBCollection coll_libri = database.getCollection("Libri");							//Mi predispongo per la manipolazione della Collection									
    	
    	BasicDBList cat = new BasicDBList();
    	cat.add("Opera Teatrale");
    	cat.add("Avventura");
    	
    	BasicDBList ris = new BasicDBList();
    	ris.add(new BasicDBObject("anno", 2010).append("formato", "Copertina Rigida"));
    	ris.add(new BasicDBObject("anno", 2015).append("formato", "Copertina Morbida"));
    	
    	DBObject libro = new BasicDBObject()									//DRM - Document Relationship Mapping
    			.append("titolo", "Guerra e Pace")
    			.append("autore", "Tolstoj")
    			.append("casa_editrice", 
    					new BasicDBObject("via", "Via della Stufa")
    					.append("citta", "Bologna")
    					)
    			.append("categorie", cat)
    			.append("ristampe", ris);
    	
    	
    	coll_libri.insert(libro);
    	
    	DBCursor cursor = coll_libri.find( new BasicDBObject("casa_editrice.citta", "Milano"));
    	
    	ArrayList<Libro> elenco = new ArrayList<Libro>();
    	 
    	for(DBObject temporaneo: cursor) {    		
    		Libro lib = new Libro();
    		lib.setTitolo((String)temporaneo.get("titolo"));
    		lib.setAutore((String)temporaneo.get("autore"));
    		
    		System.out.println(temporaneo.get("casa_editrice"));
    		
    		elenco.add(lib);
    	}
    	
        return ("Filled!");
    }
    

    @RequestMapping("check")
    public String checkTheMongo(){
    	MongoClientURI uri_connessione = new MongoClientURI("mongodb://admin:toor@mongodb-new/sampledb");	//Definire un URI di connessione
    	MongoClient client = new MongoClient(uri_connessione);								//Definisco il cliente che si occuperà di convogliare tutte le istruzioni al DB
    	
    	DB database = client.getDB("sampledb");												//use Libreria
    	DBCollection coll_libri = database.getCollection("Libri");	
    	
		DBCursor cursor = coll_libri.find( new BasicDBObject("casa_editrice.citta", "Milano"));
    	
    	ArrayList<Libro> elenco = new ArrayList<Libro>();
    	 
    	String result = "Eccolo: ";
    	for(DBObject temporaneo: cursor) {    		
    		Libro lib = new Libro();
    		lib.setTitolo((String)temporaneo.get("titolo"));
    		lib.setAutore((String)temporaneo.get("autore"));
    		
    		System.out.println(temporaneo.get("casa_editrice"));
    		
    		elenco.add(lib);
	    	result += " - " + (String)temporaneo.get("titolo");
    	}
    	
    	return result;
    }
    
//	@GetMapping("/students/")
//	public List<Course> retrieveCoursesForStudent(@PathVariable String studentId) {
//		return studentService.retrieveCourses(studentId);
//	}
}